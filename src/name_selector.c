/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <debug/sahtrace.h>

#include "name_selector.h"
#include <gmap/gmap.h>

/**********************************************************
* Macro definitions
**********************************************************/
#define GMAP_NAME_SELECTOR_DEFAULT_NAME_PREFIX_USB_HUB "usb_hub_"
#define GMAP_NAME_SELECTOR_DEFAULT_NAME_PREFIX_USB_DISK "usb_disk_"
#define GMAP_NAME_SELECTOR_DEFAULT_NAME_PREFIX_USB_HID "usb_hid_"

#define GMAP_NAME_SELECTOR_NAME_PARAMETER_NAME "Name"
#define GMAP_NAME_SELECTOR_NAME_PARAMETER_SOURCE "Source"

#define GMAP_NAME_SELECTOR_DEVICE_PARAMETER_NAME "Name"
#define GMAP_NAME_SELECTOR_DEVICE_PARAMETER_TAGS "Tags"
#define GMAP_NAME_SELECTOR_DEVICE_PARAMETER_KEY "Key"

#define ME "name_select"

#ifndef when_str_empty_status
#define when_str_empty_status(x, l, c) if(x == NULL || x[0] == '\0') { c; goto l; }
#endif

#ifndef when_null_status
#define when_null_status(x, l, c) if(x == NULL) { c; goto l; }
#endif

/**********************************************************
* Type definitions
**********************************************************/

typedef struct _gmap_mod_name_selector {
    amxd_dm_t* dm;
} gmap_mod_name_selector_t;

/**********************************************************
* Variable declarations
**********************************************************/

static gmap_mod_name_selector_t app;

/**********************************************************
* Function Prototypes
**********************************************************/

static char* select_name_network_device(amxd_object_t* const device_instance);
static char* mod_name_selector_create_default_name(amxd_object_t* const device_instance, const char* key, const char* current_name);

static char* find_name_object_with_sources(amxd_object_t* const device_instance, const char* name_sources[]);
static amxd_status_t select_name(amxd_object_t* const device_instance);
static amxd_status_t set_name(amxd_object_t* const device_instance, const char* new_name);

/**********************************************************
* Functions
**********************************************************/

static char* find_name_object_with_sources(amxd_object_t* const device_instance, const char* name_sources[]) {
    uint32_t index = 0;
    char* name = NULL;
    amxd_object_t* name_obj = NULL;

    if(device_instance != NULL) {
        while(name_sources[index]) {

            name_obj = amxd_object_findf(device_instance, "Names.[%s == '%s']", GMAP_NAME_SELECTOR_NAME_PARAMETER_SOURCE, name_sources[index]);
            if(name_obj) {
                name = amxd_object_get_cstring_t(name_obj, GMAP_NAME_SELECTOR_NAME_PARAMETER_NAME, NULL);
                if(name != NULL) {
                    break;
                }
            }
            index++;
        }
    }

    return name;
}

static char* select_name_network_device(amxd_object_t* const device_instance) {
    char* selected_name = NULL;
    const char* name_sources[] = {
        "webui",
        "user",
        "dil",
        "dect_ule",
        "zwave",
        "hue",
        "dhcp",
        NULL
    };

    if(device_instance != NULL) {
        selected_name = find_name_object_with_sources(device_instance, name_sources);
    }
    return selected_name;
}


static char* mod_name_selector_create_default_name(amxd_object_t* const device_instance, const char* key, const char* current_name) {
    static uint32_t usb_hub = 0;
    static uint32_t storage = 0;
    static uint32_t usb_hid = 0;
    char* selected_name = NULL;
    amxc_string_t* name = NULL;

    when_null(device_instance, exit_1);
    when_str_empty(key, exit_1);

    amxc_string_new(&name, 0);

    //make a name based upon tags
    if(gmap_device_hasTag(key, "usb physical hub", NULL, gmap_traverse_this)) {
        if((current_name == NULL) || (strncmp(current_name, GMAP_NAME_SELECTOR_DEFAULT_NAME_PREFIX_USB_HUB, strlen(GMAP_NAME_SELECTOR_DEFAULT_NAME_PREFIX_USB_HUB)) != 0)) {
            usb_hub++;
        }
        amxc_string_appendf(name, "%s%d", GMAP_NAME_SELECTOR_DEFAULT_NAME_PREFIX_USB_HUB, usb_hub);
    } else if(gmap_device_hasTag(key, "usb physical storage", NULL, gmap_traverse_this)) {
        if((current_name == NULL) || (strncmp(current_name, GMAP_NAME_SELECTOR_DEFAULT_NAME_PREFIX_USB_DISK, strlen(GMAP_NAME_SELECTOR_DEFAULT_NAME_PREFIX_USB_DISK)) != 0)) {
            storage++;
        }
        amxc_string_appendf(name, "%s%d", GMAP_NAME_SELECTOR_DEFAULT_NAME_PREFIX_USB_DISK, storage);
    } else if(gmap_device_hasTag(key, "usb physical hid", NULL, gmap_traverse_this)) {
        if((current_name == NULL) || (strncmp(current_name, GMAP_NAME_SELECTOR_DEFAULT_NAME_PREFIX_USB_HID, strlen(GMAP_NAME_SELECTOR_DEFAULT_NAME_PREFIX_USB_HID)) != 0)) {
            usb_hid++;
        }
        amxc_string_appendf(name, "%s%d", GMAP_NAME_SELECTOR_DEFAULT_NAME_PREFIX_USB_HID, usb_hid);
    }

    if(!amxc_string_is_empty(name)) {
        selected_name = amxc_string_dup(name, 0, amxc_string_text_length(name));
    }

exit_1:
    amxc_string_delete(&name);
    return selected_name;
}

static amxd_status_t set_name(amxd_object_t* const device_instance, const char* new_name) {
    amxd_status_t status = amxd_status_unknown_error;

    when_null_status(device_instance, exit, status = amxd_status_parameter_not_found);
    if(new_name == NULL) {
        status = amxd_status_parameter_not_found;
        goto exit;
    }

    status = amxd_object_set_cstring_t(device_instance, GMAP_NAME_SELECTOR_DEVICE_PARAMETER_NAME, new_name);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed set Device name to: %s", new_name);
        goto exit;
    }
    SAH_TRACEZ_INFO(ME, "Successfully set Device name to: %s", new_name);

exit:
    return status;
}

static amxd_status_t select_name(amxd_object_t* const device_instance) {
    char* selected_name = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    char* tags = NULL;
    char* key = NULL;
    char* current_name = NULL;

    when_null_status(device_instance, exit_1, status = amxd_status_parameter_not_found);
    key = amxd_object_get_cstring_t(device_instance, GMAP_NAME_SELECTOR_DEVICE_PARAMETER_KEY, &status);
    when_failed(status, exit_1);
    current_name = amxd_object_get_cstring_t(device_instance, GMAP_NAME_SELECTOR_DEVICE_PARAMETER_NAME, &status);
    when_failed(status, exit_2);

    tags = amxd_object_get_cstring_t(device_instance, GMAP_NAME_SELECTOR_DEVICE_PARAMETER_TAGS, &status);
    if((status != amxd_status_ok) || (tags == NULL)) {
        status = amxd_status_missing_key;
        goto exit_3;
    }
    free(tags);

    //get name for network_device
    if(gmap_device_hasTag(key, "lan mac", NULL, gmap_traverse_this)) {
        selected_name = select_name_network_device(device_instance);
    }

    //get name from default name selector, if no name was found
    if(selected_name == NULL) {
        selected_name = mod_name_selector_create_default_name(device_instance, key, current_name);
        if(selected_name == NULL) {
            selected_name = strdup("");
        }
    }

    //set name if not the current name
    status = set_name(device_instance, selected_name);

    free(selected_name);
exit_3:
    free(current_name);
exit_2:
    free(key);
exit_1:
    return status;
}

amxd_status_t _device_object_changed(const char* const sig_name, const amxc_var_t* const data, UNUSED void* const priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* device_instance = NULL;

    when_null_status(data, exit, status = amxd_status_parameter_not_found);
    when_str_empty_status(sig_name, exit, status = amxd_status_parameter_not_found);

    if(strcmp(sig_name, "dm:object-changed") != 0) {
        status = amxd_status_invalid_action;
        goto exit;
    }

    //get the device object
    device_instance = amxd_dm_signal_get_object(gmap_mod_name_selector_get_dm(), data);
    when_null_status(device_instance, exit, status = amxd_status_object_not_found);

    status = select_name(device_instance);

exit:
    return status;
}

amxd_status_t _name_instance_added_or_removed(const char* const sig_name, const amxc_var_t* const data, UNUSED void* const priv) {

    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* name_object = NULL;
    amxd_object_t* device_instance = NULL;

    when_null_status(data, exit, status = amxd_status_parameter_not_found);
    when_str_empty_status(sig_name, exit, status = amxd_status_parameter_not_found);

    if((strcmp(sig_name, "dm:instance-added") != 0) && (strcmp(sig_name, "dm:instance-removed") != 0)) {
        status = amxd_status_invalid_action;
        goto exit;
    }

    //get the name object
    name_object = amxd_dm_signal_get_object(gmap_mod_name_selector_get_dm(), data);
    when_null_status(name_object, exit, status = amxd_status_object_not_found);

    //get the device object
    device_instance = amxd_object_get_parent(name_object);
    when_null_status(device_instance, exit, status = amxd_status_object_not_found);

    status = select_name(device_instance);
exit:
    return status;
}

amxd_dm_t* gmap_mod_name_selector_get_dm(void) {
    return app.dm;
}

int _gmap_mod_name_selector_main(int reason, amxd_dm_t* dm, UNUSED amxo_parser_t* parser) {
    int retval = 0;
    switch(reason) {
    case 0:
        app.dm = dm;
        gmap_client_init(amxb_be_who_has("Devices.Device"));
        SAH_TRACEZ_INFO(ME, "Starting _gmap_mod_name_selector_main");
        break;
    case 1:
        SAH_TRACEZ_INFO(ME, "Stopping _gmap_mod_name_selector_main");
        break;
    }

    return retval;
}
